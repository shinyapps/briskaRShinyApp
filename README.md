[![pipeline status](https://gitlab.paca.inrae.fr/vbaudrot/r-shiny-app-ci-cd/badges/master/pipeline.svg)](https://gitlab.paca.inrae.fr/vbaudrot/r-shiny-app-ci-cd/commits/master)


## Inline Shiny App

[Inline briskaR App](https://shiny.biosp.inrae.fr/app/briskaR)

## Authors

* [Virgile Baudrot](https://www.researchgate.net/profile/Virgile_Baudrot)
* [Jean-François Rey](https://jeff.biosp.org) \<jean-francois.rey at inra.fr\>

## License

See [LICENSE](LICENSE)

## Run localy Docker App image

Either downloads the artifact `briskaRshinyApp_*.tar.gz` or from the folder `briskaRshinyApp`,
build the Archive of the package `briskaRshinyApp_*.tar.gz`.

To build the archive, run command lines:

```
R CMD build --no-build-vignettes --no-manual briskaRshinyApp/
mv briskaRshinyApp_*.tar.gz briskaRshinyApp/briskaRshinyApp_*.tar.gz
```

Then run (before move within folder `briskaRshinyApp/`):

```
sudo docker build --build-arg APP_NAME=briskaRapp --tag briskarshinyapp:latest -f Dockerfile .
```

Check images build 

```
sudo docker images
```

One of the line should look like this:
```
REPOSITORY                                       TAG                 IMAGE ID            CREATED              SIZE
briskarshinyapp                                  latest              13d38651faf5        About a minute ago   2.89GB
```

Get and execute localy the docker image of the app.

```
sudo docker run -p 3838:3838 --name briskaRapp briskarshinyapp:latest 
```
The application is then accessible at : [http://localhost:3838](http://localhost:3838)  


To stop and remove container

```
sudo docker stop briskaRapp
sudo docker container rm briskaRapp
```

## Regenerate Docker image


From the folder `briskaRshinyApp`, build the Archive of the package ` briskaRshinyApp_*.tar.gz`.

Then run:

```
sudo docker build --build-arg APP_NAME=briskaRapp --tag briskarshinyapp:latest -f Dockerfile .
```

or build Docker image from gitlab repository:

```
sudo docker build --build-arg APP_NAME=briskaRapp --cache-from gitlab.paca.inra.fr:4567/shinyapps/briskarshinyapp:latest --tag briskarshinyapp:latest -f Dockerfile .
```

## Use Docker image localy

```sh
sudo docker run -it -d -p 3838:3838 --name briskarshinyapp gitlab.paca.inrae.fr:4567/shinyapps/briskarshinyapp:latest
```
open [localhost:3838](localhost:3838)
```sh
# stop container
sudo docker stop briskarshinyapp
sudo docker rm briskarshinyapp
```