# Module UI
  
#' @title   mod_tabItemEmission_ui and mod_tabItemEmission_server
#' @description  A shiny Module.
#'
#' @param id shiny id
#' @param input internal
#' @param output internal
#' @param session internal
#'
#' @rdname mod_tabItemEmission
#'
#' @keywords internal
#' @export 
#' @importFrom shiny NS tagList 
mod_tabItemEmission_ui <- function(id){
  ns <- NS(id)
  # tagList(
  # Leave this function for adding external resources
  # golem_add_external_resources(),
  # List the first level UI elements here 
  tabItem(tabName = "emission",
          # App title ----
          briskaRshinyApp::headerNavigationPage("backDispersalTab", "switchHostTab"),
          fluidRow(
            shinydashboardPlus::gradientBox(
              title = iconed(" - Information", "info"),
              width = 12,
              gradientColor = "purple", 
              boxToolSize = "xs", 
              collapsible = TRUE,
              closable = FALSE,
              footer_padding = FALSE,
              h4("What to do here?"),
              tags$ul(
                tags$li("(1) check the source and the dispersal are well defined (OK in the check list),"), 
                tags$li("(2) generate or load an emission profile for each source fields,"), 
                tags$li("(3) generate or load deposition processes,"),
                tags$li("(4) run convolution of dispersal with emission profile by clicking on 'Run'.")
              ),
              h4("Check list of previous items"),
              verbatimTextOutput(ns("CheckEmission")),
              hr(),
              h4("Computing convolution dispersal and emission"),
              p("Once all elements in Check list is OK, this part allows to compute dispersal probability."),
              tags$video(id = ns("videoEmission"),
                         type = "video/mp4",
                         src = "www/emission_capture2.mp4",
                         controls = "controls",
                         height = "300px")
            )
          ),
          fluidRow( # width of fluidrow is 12
            box(title = iconed("Pollen Emission", "database"),
                solidHeader = TRUE,
                width = 8,
                status = "warning",
                p("You can either generate or load an emission profile."),
                column(width = 6,
                       tabsetPanel(id = ns("tabsetEmission"),
                                   selected = "GenerateEmission",
                                   tabPanel(title = "Generate",
                                            value = "GenerateEmission",
                                            p("To generate the emission profiles, select a period of pollen emission.
                                               Then, define shape and scale of emission functions."),
                                            dateRangeInput(inputId = ns("periodEMISSION"),
                                                           label = "Period - minimum 14 days",
                                                           start = "2006-06-15", # Sys.Date()-10,
                                                           end = "2006-08-15",# Sys.Date()+10,
                                                           format = "dd/mm/yyyy",
                                                           separator = " to "),
                                            p("Pollen quantity is defined in pollen grain per square meter.
                                              Pollen quantity = density * quantity * daily proportion."),
                                            p("Density is a uniform law in[7,11].Daily proportion is provided by the following curve:"),
                                            plotOutput(ns("plotEMISSIONprofile"), height = "100px"),
                                            p("Quantity is define with shape and scale of a gamma distribution:
                                              mean=shape*scale and variance=shape*scale^2"),
                                            numericInput(inputId = ns("pollenSHAPE"),
                                                         label = "Pollen shape",
                                                         value = 1.6,
                                                         min = 0, max = 10),
                                            numericInput(inputId = ns("pollenSCALE"),
                                                         label = "Pollen scale",
                                                         value = 1*10^6#,
                                                         #in = 0, max = 1
                                                         ),
                                            br(),
                                            actionButton(inputId = ns("goEmission"),
                                                         label =  "Valid",
                                                         icon = icon("check"),
                                                         style="color: #fff; background-color: #00a65a; border-color: #00a65a")
                                   ),
                                   tabPanel(title = "Load",
                                            value = "LoadEmission",
                                            # ---
                                            p("If you have your own pollen emission profiles, load the CSV file.
                                              To see how the CSV file should look like, download the example file we provide."),
                                            fileInput("sourceEMISSION",
                                                      'Choose CSV file - EMISSION',
                                                      accept=c('text/csv', 'text/comma-separated-values,text/plain')),
                                            hr(),
                                            p("Download and upload an example"),
                                            downloadButton(ns("download_sourceEMISSION"),
                                                           "Download Emission Profile",
                                                           style="color: #fff; background-color: #33595f; border-color: #052327")
                                   )
                       )
              ),
              column(width = 6,
                     tags$ul(
                       tags$li(strong("x-axis is the date along the emission timeline,")), 
                       tags$li(strong("y-axis is the amount of pollen emit.")), 
                     ),
                     textInput(inputId = ns("unitPollen"),
                               label = "Unit of pollen emission",
                               value = "pollen / m^2"),
                     plotOutput(ns("plotEMISSION"))
              )
            ),
            box(title = iconed("Pollen Deposition", "sliders-h"),
                solidHeader = TRUE,
                width = 4,
                status = "warning",
                p("Adherence is the proportion of pollen in the air column over a leaf that is daily deposited on the leaf."),
                numericInput(inputId = ns("betaSPREAD"),
                             label = "Adherence",
                             value = 0.2,
                             min = 0, max = 1),
                h4("Loss rate at landscape scale:"),
                p("Loss rate is the fraction of pollen deposited on the leaf that is removed every day.
                  This rate of removal is applied at the landscape scale. You can choose a fixed removal rate or
                  use a function of rainfall time serie."),
                tabsetPanel(id = ns("tabsetRainfall"),
                            selected = "GenerateRainfall",
                            tabPanel(title = "Generate",
                                     value = "GenerateRainfall",
                                     numericInput(inputId = ns("lossSPREAD"),
                                                  label = "Global loss rate",
                                                  value = 0.1,
                                                  min = 0, max = 1)
                            ),
                            tabPanel(title = "Load",
                                     value = "LoadRainfall",
                                     # ---
                                     p("alpha(z) is the loss rate depending on a rainfall time serie z. In the following equation,
                                     alpha_min and alpha_max are the minimum and maximum fractions of pollen that are lost and 
                                     z_alpha is the level of precipitation beyond which the loss saturates"),
                                     withMathJax(
                                       helpText('$$\\alpha(z) = \\max \\left( \\alpha_{min} + \\frac{\\alpha_{max} - \\alpha_{min}}{z_{\\alpha}} z ; \\alpha_{max} \\right) $$')
                                     ),
                                     fileInput("timeserieRAINFALL",
                                               'Choose CSV file - RAINFALL',
                                               accept=c('text/csv', 'text/comma-separated-values,text/plain')),
                                     sliderInput(inputId = ns("lossSPREADload"),
                                                 label = "Loss rate- [alpha_min,alpha_max]",
                                                 min = 0, max = 1,
                                                 value = c(0,1)),
                                     numericInput(inputId = ns("lossThresholdSPREADload"),
                                                  label = "Thershold for 100% loss - z_alpha",
                                                  value = 30,
                                                  min = 0),

                                     hr(),
                                     downloadButton(ns("download_timeserieRAINFALL"),
                                                    "Download Rainfall Profile",
                                                    style="color: #fff; background-color: #33595f; border-color: #052327")
                            )
                ),
                br(),
                actionButton(ns("goRunSPREAD"),
                             "Run",
                             icon = icon("rocket"),
                             style="color: #fff; background-color: #dd4b39; border-color: #dd4b39")
                # style="color: #fff; background-color: #00a65a; border-color: #00a65a")
                
            )
          ),
          fluidRow( # width of fluidrow is 12
            box(title =iconed("Deposition", "cogs"),
                solidHeader = TRUE,
                width = 12, status = "success",
                p("For each date along the emission period you generated or uploaded previously in the item 'Emission',
                    the convolution is computed according to the kernel which has been selected."),
                tags$ul(
                  tags$li(strong("Check for each date along the emision period, the emission at the landscape level.")),
                  tags$li(strong("The unit is the one you provided within the 'Emission panel'."))
                ),
                box(solidHeader = FALSE,
                    width = 7, status = "success",
                       h4("Plot only the 9 first fields"),
                       plotOutput(ns("plotRunSPREAD"))
                ),
                box(title = "Specific Plot",
                    solidHeader = FALSE,
                    width = 5, status = "success",
                    selectizeInput(ns("numPlotSPREAD"),
                                   label = "Select a layer to plot",
                                   choices = NULL,
                                   options = list(
                                     placeholder = 'Please select column below',
                                     onInitialize = I('function() { this.setValue(""); }')
                                   )
                    ),
                    # actionButton(ns("goPlotSPREAD"), "Plot"),
                    # plot the Figure
                    plotOutput(ns("plotSPREAD"))
                )
            )
          ),
          briskaRshinyApp::footerPage()
  )
}
    
# Module Server
#' @rdname mod_tabItemEmission
#' @export
#' @keywords internal
    
mod_tabItemEmission_server <- function(input, output, session, r){
  ns <- session$ns
  ### CHECKING
  output$CheckEmission <- renderText({
    CheckEmission = list()
    CheckEmission$landscapeSOURCE= ifelse(is.null(r$landscapeSOURCE), "NO Source", "Source OK")
    CheckEmission$stack_dispersal = ifelse(is.null(r$stack_dispersal), "NO Dispersal", "Dispersal OK")
    return(HTML(paste0(
      "1. ", CheckEmission$landscapeSOURCE, ",\n",
      "2. ", CheckEmission$stack_dispersal, "."))
      )
  }) 
  ### Download Example File
  output$download_sourceEMISSION <- downloadHandler(
    filename = function() {
      paste("sourceEMISSION_", Sys.Date(), ".csv", sep = "")
    },
    content = function(file) {
      readr::write_csv(briskaRshinyApp::sourceEMISSION, file)
    }
  )
  output$plotEMISSIONprofile <- renderPlot({
    ggplot() + 
      theme_minimal() +
      labs(x = "Time step", y = paste("Daily proportion")) +
      geom_point(aes(x = 1:12,
                     y = c(0.0165, 0.0660, 0.1545, 0.1885, 0.1735, 0.1560, 0.1159, 0.0670, 0.0377, 0.0167, 0.0055, 0.0022)))
    
  })
  #### GENERATE
  # eventReactive(input$goEmission,{
  sourceEMISSION <- eventReactive(input$goEmission, {
    # proportionPOLLEN = briskaRshinyApp::proportionPOLLEN
    proportionPOLLEN = c(0.0165, 0.0660, 0.1545, 0.1885, 0.1735, 0.1560, 0.1159, 0.0670, 0.0377, 0.0167, 0.0055, 0.0022)

    funTimePollen <- function(time, pollenShape, pollenScale){
      density = runif(1, 7, 11)
      pollen = rgamma(1, shape = pollenShape, scale = pollenScale)
      nbr_days = length(time)
      deb = sample(1:(nbr_days - length(proportionPOLLEN)), 1)
      end = (deb + length(proportionPOLLEN) - 1)
      pollen_emission <- rep(0, nbr_days)
      pollen_emission[deb:end] <- as.numeric(pollen * density * proportionPOLLEN)
      return(pollen_emission)
    }

    sourceEMISSIONlist = dplyr::tibble(
      Date = lapply(1:nrow(r$landscapeSOURCE), function(i){ seq(min(input$periodEMISSION), max(input$periodEMISSION), by = "day")})
    )
    if(is.null(r$landscapeSOURCE$IDsource)){
      sourceEMISSIONlist$IDsource = 1:nrow(sourceEMISSIONlist)
    } else{
      sourceEMISSIONlist$IDsource = r$landscapeSOURCE$IDsource
    }
   
    sourceEMISSIONlist$EMISSION = lapply(1:nrow(sourceEMISSIONlist), function(i){
      # deb = sample(seq(min(input$periodEMISSION), max(input$periodEMISSION), by = "day"), nrow(r$landscapeSOURCE), replace = TRUE)

      funTimePollen(sourceEMISSIONlist$Date[[i]], #seq(deb, max(input$periodEMISSION), by = "day"),
                    pollenShape = input$pollenSHAPE, pollenScale = input$pollenSCALE)
    })

    sourceEMISSION = sourceEMISSIONlist %>%
      tidyr::unnest(cols = c("Date", "EMISSION"))

    return(sourceEMISSION)

  })

  # ---------------------------------------------------------------------------------
  observe({
    r$sourceEMISSION = sourceEMISSION()
    r$stackTimelineEMISSION <- sort(unique(sourceEMISSION()[["Date"]]))
  })

  observe({
    r$unitPollen = input$unitPollen
  })
  
  output$plotEMISSION <- renderPlot({
    if(!is.null(r$sourceEMISSION)){
      # input$goSourceEMISSION
      # isolate({
        ggplot(data = r$sourceEMISSION,
               aes(x = Date,
                   y = EMISSION,
                   group = IDsource,
                   color = IDsource)) +
          theme_minimal() +
          theme(legend.position="none") + # remove the legend
          labs(y = paste("Pollen emission in", input$unitPollen)) + 
          geom_line()
      # })
    }
  })
  # ###------------------------- SPREAD
  stack_exposure <- eventReactive(input$goRunSPREAD, {

    showModal(modalDialog(title = "Run in progress...",
                          "It can take a long time",
                          easyClose = TRUE,
                          footer="blablabla"))

    # sourceEMISSION_stack <- fd_stack(r$sourceEMISSION, key = "IDsource")
    sourceEMISSION_stack = r$sourceEMISSION %>%
      dplyr::group_by(IDsource) %>%
      dplyr::arrange(Date) %>%
      dplyr::summarize(Date = list(Date),
                       EMISSION = list(EMISSION))
    
    stack_exposure <- briskaR::brk_exposure( r$stack_dispersal,
                                             sourceEMISSION_stack,
                                             key = "EMISSION", # Name of the new column
                                             keyTime = "Date", # Length of the reference timeline
                                             loss = input$lossSPREAD,
                                             beta = input$betaSPREAD,
                                             quiet = TRUE)

    removeModal()

    return(stack_exposure)
  })
  observe({
    r$stack_exposure <- stack_exposure()
  })
  # ### SPECIFIC PLOT
  output$plotRunSPREAD <- renderPlot({
    input$goRunSPREAD
    isolate({
      if(length(stack_exposure() >= 9)){
        plot( stack_exposure()[[1:9]], main = paste("Unit is:", r$unitPollen))
      } else{
        plot(stack_exposure()[[1:length(stack_exposure())]], main = paste("Unit is:", r$unitPollen))
      }
    })
  })
  observe({
    numPlotSPREAD <- names(stack_exposure())
    updateSelectizeInput( session, "numPlotSPREAD", selected = NULL,  choices = numPlotSPREAD)
  })
  output$plotSPREAD <- renderPlot({
    # input$goPlotSPREAD
    input$numPlotSPREAD
    isolate({
      plot(stack_exposure()[[input$numPlotSPREAD]], main = paste("Unit is:", r$unitPollen))
    })
  })
}
    
## To be copied in the UI
# mod_tabItemEmission_ui("tabItemEmission_ui_1")
    
## To be copied in the server
# callModule(mod_tabItemEmission_server, "tabItemEmission_ui_1")
 
