# Module UI
  
#' @title   mod_tabItemDispersal_ui and mod_tabItemDispersal_server
#' @description  A shiny Module.
#'
#' @param id shiny id
#' @param input internal
#' @param output internal
#' @param session internal
#'
#' @rdname mod_tabItemDispersal
#'
#' @keywords internal
#' @export 
#' @importFrom shiny NS tagList 
mod_tabItemDispersal_ui <- function(id){
  ns <- NS(id)
  tabItem(tabName = "dispersal",
          title = "Dispersal",
          # App navigation ----
          briskaRshinyApp::headerNavigationPage("backLandscapeTab", "switchEmissionTab"),
          fluidRow(
            shinydashboardPlus::gradientBox(
              title = iconed(" - Information", "info"),
              width = 12,
              gradientColor = "purple", 
              boxToolSize = "xs", 
              collapsible = TRUE,
              closable = FALSE,
              footer_padding = FALSE,
              h4("What to do here?"),
              tags$ul(
                tags$li("(1) check the maize fields and the frame are well defined (OK in the check list),"), 
                tags$li("(2) choose a kernel function and provide parameter values,"), 
                tags$li("(3) compute the dispersal by clicking on 'Run'.")
              ),
              h4("Check list of previous items"),
              p("Please, for each step of the app, check that all items of the list are ok."),
              verbatimTextOutput(ns("CheckDispersal")),
              hr(),
              h4("How to run a dispersal kernel"),
              p("Once all elements in Check list is OK, this part allows to compute dispersal probability. 
                Here, for each source field uploaded previously, the dispersal of source is computed according to the selected kernel."),
               tags$video(id = ns("videoDispersal"),
                          type = "video/mp4",
                          src = "www/dispersal_capture2.mp4",
                          controls = "controls",
                          height = "300px")
            )
          ),
          fluidRow( # width of fluidrow is 12
            box(title = iconed("Frame", "database"),
                solidHeader = TRUE,
                width = 5, status = "warning",
                p("Once you have generated or loaded the source and host, you have to define the landscape borders:
                    a square surrounding both sources and hosts fields. The square shape is required for dispersal and convolution computing."),
                p("Again, you can either generate a frame by providing a buffer distance, or load your own frame. Make sure the frame you upload is a square."),
                tabsetPanel(id = ns("tabsetSQUAREFRAME"),
                            selected = "bufferSQUAREFRAME",
                            tabPanel(title = "Generate",
                                     value = "bufferSQUAREFRAME",
                                     p("Choose the size of the buffer, and click on the green button 'Valid'."),
                                     strong("Make sure your landscape is in metric system. If you used 'Generate' the landscape is fine.
                                            Otherwise, if you are not sure, see previous step 'landscape'."),
                                     numericInput(inputId = ns("squareFrameBuffer"),
                                                  label = "Size of the buffer [in meter]",
                                                  value = 100),
                                     actionButton(inputId = ns("goValidBufferSQUAREFRAME"),
                                                  label =  "Valid",
                                                  icon = icon("check"),
                                                  style="color: #fff; background-color: #00a65a; border-color: #00a65a")
                            ),
                            tabPanel(title = "Load",
                                     value = "loadSQUAREFRAME",
                                     fileInput("landscapeSQUAREFRAME",
                                               "Choose a KML File for Square Frame"),
                                     textOutput(ns("textEPSGsqFrame")),
                                     selectizeInput(ns("ctrlEPSGsqFrame"),
                                                    label = "Choose EPSG",
                                                    choices = NULL),
                                     hr(),
                                     p("You can download and upload the following example.
                                        When using this example,", strong("change EPSG to 2154")),
                                     downloadButton(ns("download_LandscapeSQUAREFRAME"),
                                                    "Download Frame shapefile",
                                                    style="color: #fff; background-color: #33595f; border-color: #052327")
                            )
                ),
                plotOutput(ns("plotLANDSCAPEframe"))
            ),
            box(title = iconed("Kernel", "sliders-h"),
                solidHeader = TRUE,
                width = 7, status = "warning",
                #
                p("The shapefile provide previously needs to be convert into a raster.
                Increasing this value can dramatically increase the computing time. So we fix maximum at 1024.
               However, you have to keep in mind that resolution is the length of landscape border (previously defined by the square frame)
               devided by the number of pixel (max 1024). For better resolution, you can upload the shiny App on your own machine."),
                shinyWidgets::sliderTextInput(inputId = ns("kernelSizeRaster"),
                                "Number of pixels along one side:",
                                choices=2^(2:10),
                                selected=2^8, grid = TRUE),
                actionButton(
                  inputId = ns("info_kernelSizeRaster"),
                  label = " - Resolution: pixel size",
                  icon = icon("question-circle"),
                  style="color: #fff; background-color: #605ca8; border-color: #605ca8"
                ),
                hr(),
                #
                strong("Kernel selection and parameterization"),
                p("Default kernel is Geometric with parameter at -2.63 considering a metric system (default is Lambert-93 valid in mainland France).
                  If you use an other projection system (validated in previous landscape panel), you should provide different parameterization."),
                # checkboxInput(ns("checkEPSGdispersal"),
                #               label = "Lambert-93, EPSG:2154", value = TRUE),
                actionButton(
                  inputId = ns("info_kernel"),
                  label = " - Dispersal kernel",
                  icon = icon("question-circle"),
                  style="color: #fff; background-color: #605ca8; border-color: #605ca8"
                ),
                tabsetPanel(id = ns("tabsetDispersal"),
                            # geometric
                            tabPanel(title = "Geometric",
                                     value = "kernelGeometric",
                                     withMathJax(
                                       helpText('$$\\frac{1}{(\\sqrt{x^2 + y^2})^\\beta} \\times \\frac{(\\beta + 1)( \\beta + 2)}{2 \\pi}$$')
                                      ),
                                     numericInput(inputId = ns("kernelGeometric_beta"),
                                                  label = "parameter beta",
                                                  value = -2.63)
                            ),
                            # NIG
                            tabPanel(title = "NIG",
                                     value = "kernelNIG",
                                     withMathJax(
                                       helpText("with \\( p = b_3^2 + b_1^2 \\cos^2(\\theta) + b_2 \\sin^2(\\theta) \\) and \\( q = 1 + a_1^2 x^2 + a_2^2 y^2 \\), NIG is given by:")
                                     ),
                                     withMathJax(
                                       helpText("$$\\frac{ a_1 a_2 \\exp(b_3)}{2 \\pi} \\times \\frac{q^{-1/2} + p^{1/2}}{q} \\times
                                                \\exp(- \\sqrt{q p}) \\times \\exp(a_1 b_1 \\cos(\\theta) x + a_2 b_2 \\sin(\\theta) y)$$")
                                     ),
                                     numericInput(inputId = ns("kernelNIG_a1"),
                                                  label = "parameter a1",
                                                  value = 0.2073),
                                     numericInput(inputId = ns("kernelNIG_a2"),
                                                  label = "parameter a2",
                                                  value = 0.2073),
                                     numericInput(inputId = ns("kernelNIG_b1"),
                                                  label = "parameter b1",
                                                  value = 0.3971),
                                     numericInput(inputId = ns("kernelNIG_b2"),
                                                  label = "parameter b2",
                                                  value = 0.3971),
                                     numericInput(inputId = ns("kernelNIG_b3"),
                                                  label = "parameter b3",
                                                  value = 0.0649),
                                     numericInput(inputId = ns("kernelNIG_theta"),
                                                  label = "parameter theta",
                                                  value = 0)
                            ),
                            # Student
                            tabPanel(title = "Student",
                                     value = "kernelStudent",
                                     withMathJax(
                                       helpText("$$\\frac{b-1}{a^2 \\pi} \\left( 1 + \\frac{x^2 + y^2}{a^2} \\right)^{-b} \\exp \\left( c_1 \\cos ( \\theta - c_2 ) \\right) $$")
                                     ),
                                     numericInput(inputId = ns("kernelStudent_a"),
                                                  label = "parameter a",
                                                  value = 1.55),
                                     numericInput(inputId = ns("kernelStudent_b"),
                                                  label = "parameter b",
                                                  value = 1.45),
                                     numericInput(inputId = ns("kernelStudent_c1"),
                                                  label = "parameter c1",
                                                  value = 1.55),
                                     numericInput(inputId = ns("kernelStudent_c2"),
                                                  label = "parameter c2",
                                                  value = 1.55),
                                     numericInput(inputId = ns("kernelStudent_theta"),
                                                  label = "parameter theta",
                                                  value = 0)
                            ),
                            # Fat tail
                            tabPanel(title = "Fat Tail",
                                     value = "kernelFatTail",
                                     withMathJax(
                                       helpText('$$a \\left( \\sqrt{x^2 + y^2} \\right)^b $$')
                                     ),
                                     numericInput(inputId = ns("kernelFatTail_a"),
                                                  label = "parameter a",
                                                  value = 1.271e6),
                                     numericInput(inputId = ns("kernelFatTail_b"),
                                                  label = "parameter b",
                                                  value = -0.585)
                            ),
                            # gaussian
                            tabPanel(title = "Gaussian",
                                     value = "kernelGaussian",
                                     withMathJax(
                                       helpText('$$\\frac{1}{a^2 \\pi} \\exp \\left( - \\frac{x^2 + y^2}{a^2} \\right) $$')
                                     ),
                                     numericInput(inputId = ns("kernelGaussian_a"),
                                                  label = "parameter a",
                                                  value = 1),
                                     numericInput(inputId = ns("kernelGaussian_b"),
                                                  label = "parameter b",
                                                  value = 1)
                            ),
                            # negative exponential
                            tabPanel(title = "(Negative) Exponential",
                                     value = "kernelNegExp",
                                     withMathJax(
                                       helpText('$$\\frac{1}{2 a^2 \\pi} \\left( - \\frac{ \\sqrt{x^2 + y^2}} {a} \\right) $$')
                                     ),
                                     numericInput(inputId = ns("kernelNegExp_a"),
                                                  label = "parameter a",
                                                  value = 1)
                            ),
                            # bivariate student
                            tabPanel(title = "Bivariate Student",
                                     value = "kernel2Dt",
                                     withMathJax(
                                       helpText('$$\\frac{b-1}{a^2 \\pi} \\left( 1 + \\frac{x^2 + y^2}{a^2} \\right)^{-b} $$')
                                     ),
                                     numericInput(inputId = ns("kernel2Dt_a"),
                                                  label = "parameter a",
                                                  value = 1),
                                     numericInput(inputId = ns("kernel2Dt_b"),
                                                  label = "parameter b",
                                                  value = 1)
                            ),
                            # inverse power law
                            tabPanel(title = "Power Law",
                                     value = "kernelInvPowLaw",
                                     withMathJax(
                                       helpText('$$\frac{(b-1) (b-2)}{2 a^2 \\pi} \\left( 1 + \\frac{ \\sqrt{x^2 + y^2}}{a} \\right)^{-b} $$')
                                     ),
                                     numericInput(inputId = ns("kernelInvPowLaw_a"),
                                                  label = "parameter a",
                                                  value = 1),
                                     numericInput(inputId = ns("kernelInvPowLaw_b"),
                                                  label = "parameter b",
                                                  value = 1)
                            ),
                            # weibull
                            tabPanel(title = "Weibull",
                                     value = "kernelWeibull",
                                     withMathJax(
                                       helpText('$$\\frac{b}{2 a^2 \\pi} \\left(x^2 + y^2 \\right)^{b-2} \\exp \\left( - \\frac{\\sqrt{x^2 + y^2} ^{b} }{a^b} \\right)$$')
                                     ),
                                     numericInput(inputId = ns("kernelWeibull_a"),
                                                  label = "parameter a",
                                                  value = 1),
                                     numericInput(inputId = ns("kernelWeibull_b"),
                                                  label = "parameter b",
                                                  value = 1)
                            ),
                            # loch-sech
                            tabPanel(title = "Loch-Sech",
                                     value = "kernelLochSech",
                                     withMathJax(
                                       helpText('$$\\frac{1}{b \\pi (x^2 + y^2)} \\times
                                                \\frac{1}{ \\left( \\frac{\\sqrt{x^2 + y^2}}{a} \\right)^{1/b} + \\left( \\frac{\\sqrt{x^2 + y^2}}{a} \\right)^{-1/b} } $$')
                                     ),
                                     numericInput(inputId = ns("kernelLochSech_a"),
                                                  label = "parameter a",
                                                  value = 1),
                                     numericInput(inputId = ns("kernelLochSech_b"),
                                                  label = "parameter b",
                                                  value = 1)
                            )
                ),
                hr(),
                actionButton(inputId = ns("goRunDispersal"),
                             label =  "Run",
                             icon = icon("rocket"),
                             style="color: #fff; background-color: #dd4b39; border-color: #dd4b39")
            )
          ),
          fluidRow(
            box(title = iconed("Dispersal Probability of the source fields", "cogs"),
                solidHeader = TRUE,
                width = 12, status = "success",
                p("For each maize field uploaded previously in the item 'landscape',
                    the dispersal is computed according to the kernel which has been selected."),
                tags$ul(
                  tags$li(strong("Check for each maize fields, the dispersal kernel applied. Unit is a probability measure,")),
                  tags$li(strong("Name 'subdomain' refers to a single maize field,")),
                  tags$li(strong("left: picture represents the 9 (or less) first maize fields,")), 
                  tags$li(strong("right: select a field to represent it (if error message is printed, don't worry about it)."))
                ),
                box(
                  label = "Plot only the 9 first maize fields",
                  solidHeader = FALSE,
                  width = 8, status = "success",
                  plotOutput(ns("plotRunDISPERSAL"))
                ),
                box(
                  solidHeader = FALSE,
                  width = 4, status = "success",
                  selectizeInput(ns("numPlotDISPERSAL"),
                                 label = "Select a layer to plot",
                                 choices = NULL,
                                 options = list(
                                   placeholder = 'Please select column below',
                                   onInitialize = I('function() { this.setValue(""); }')
                                 )
                  ),
                  # plot the Figure
                  plotOutput(ns("plotDISPERSAL"))
                ),
                strong("Once graphics appears here, go to the Top of the page and click 'Next'.")
            )
          ),
          briskaRshinyApp::footerPage()
  )
}
    
# Module Server
    
#' @rdname mod_tabItemDispersal
#' @export
#' @keywords internal
    
mod_tabItemDispersal_server <- function(input, output, session, r){
  ns <- session$ns
  ### CHECKING
  output$CheckDispersal <- renderText({
    CheckDispersal = list()
    CheckDispersal$landscapeSOURCE= ifelse(is.null(r$landscapeSOURCE), "NO Source", "Source OK")
    #CheckDispersal$landscapeSQUAREFRAME= ifelse(is.null(r$landscapeSQUAREFRAME), "NO Frame", "Frame OK")
    return(HTML(paste0(
      "1. ", CheckDispersal$landscapeSOURCE, "."))
    )
  }) 
  #######################################################################################
  # >>> INFORMATION 
  observeEvent(input$info_kernelSizeRaster, {
    sendSweetAlert(
      session = session,
      title = "Information on pixel size",
      text = tags$span(
        "The shapefile provide previously needs to be", tags$b("convert into a raster"),
        "in order to compute", tags$b("dispersal map"),
        tags$br(),
        "Default value is 2^8.",
        tags$br(),
        p("Increasing this value can dramatically increase the computing time. So we fix maximum at 1024.
           However, you have to keep in mind that resolution is the length of landscape border (previously defined by the square frame)
           devided by the number of pixel (max 1024). For better resolution, you can upload the shiny app on your own machine.")
      ),
      html = TRUE
    )
  })
  observeEvent(input$info_kernel, {
    sendSweetAlert(
      session = session,
      title = "Information on kernel",
      text = tags$span(
        "The dispersal of contaminants or individuals is implemented by rastering the landscape and by computing
        the convolution between maize pollen emissions and a dispersal kernel."
      ),
      html = TRUE
    )
  })
  ############################################ SQUARE FRAME
  output$download_LandscapeFRAME <- downloadHandler(
    filename = function() {
      paste("landscapeSQUAREFRAME_", Sys.Date(), ".kml", sep = "")
    },
    content = function(file) {
      sf::st_write(briskaRshinyApp::landscapeSQUAREFRAME, file)
    }
  )
  observe({
    ctrlEPSGsqFrame <- names(r$landscapeSQUAREFRAME)
    updateSelectizeInput( session, "ctrlEPSGsqFrame", selected = "initial", choices = c("initial",rgdal::make_EPSG()$code))
  })
  observe({
    r$tabsetSQUAREFRAME <- input$tabsetSQUAREFRAME
  })
  observe({
    r$ctrlEPSGsqFrame <- input$ctrlEPSGsqFrame
  })
  observe({
    r$squareFrameBuffer <- input$squareFrameBuffer
  })
  
  observeEvent(input$goValidBufferSQUAREFRAME,{
    if(!is.null(r$landscapeSOURCE) || !is.null(r$landscapeHOST)){
      r$landscapeSQUAREFRAME <- st_squared_geometry(list(r$landscapeSOURCE, r$landscapeHOST), buffer = r$squareFrameBuffer)
    }
  })
  observe({
    if (r$tabsetSQUAREFRAME == "loadSQUAREFRAME") {
      if(!is.null(r$landscapeSQUAREFRAME)){
        if(r$ctrlEPSGsqFrame != "initial"){
          r$landscapeSQUAREFRAME <- sf::st_transform(r$landscapeSQUAREFRAME, crs = as.numeric(r$ctrlEPSGsqFrame))
        }
      }
    }
  })
  ## OUTPUT
  output$textEPSGsqFrame = renderText({
    paste("EPSG initial:", sf::st_crs(r$landscapeSQUAREFRAME$epsg))
  })
  output$plotLANDSCAPEframe <- renderPlot({
    ggplot() +
      theme_minimal() +
      geom_sf(data = r$landscapeSQUAREFRAME, fill = NA) +
      geom_sf(data = r$landscapeSOURCE, alpha = 0.5, fill = "red", color = "red") +
      geom_sf(data = r$landscapeHOST, alpha = 0.5, fill = "blue", color = "blue")
  })
  
  # INFORMATION <<<
  #######################################################################################
  # COMPUTE DISPERSAL
  stack_dispersal <- eventReactive(input$goRunDispersal, {
    # Show a modal when the button is pressed
    showModal(modalDialog(title = "Run in progress...",
                          "It can take a long time",
                          easyClose = TRUE,
                          footer="...running..."))
    
    # if(input$checkEPSGdispersal == TRUE){
    #   landscapeSOURCE_dispersal <- sf::st_transform(r$landscapeSOURCE, crs = 2154)
    #   landscapeSQUAREFRAME_dispersal <- sf::st_transform(r$landscapeSQUAREFRAME, crs = 2154)
    #   tolerance_square = 1 #transformation may change the square-frame so 100% tolerance
    # } else{
      landscapeSOURCE_dispersal <- r$landscapeSOURCE
      landscapeSQUAREFRAME_dispersal <- r$landscapeSQUAREFRAME
      tolerance_square = 0.1
    # }
    
    kernelSWITCH = switch(input$tabsetDispersal,
                    "kernelGeometric" = "geometric",
                    "kernelNIG" = "NIG",
                    "kernelStudent" = "student",
                    "kernelFatTail" = "kernel_fat_tail",
                    "kernelGaussian" = "gaussian",
                    "kernelNegExp" = "negExponential",
                    "kernel2Dt" = "k2Dt",
                    "kernelInvPowLaw" = "invPowerLaw",
                    "kernelWeibull" = "weibull",
                    "kernelLochSech" = "lochSech"
                    )
    
    kernel.optionsSWITCH = switch(input$tabsetDispersal,
           "kernelGeometric" = list("a" =  input$kernelGeometric_beta),
           "kernelNIG" = list("a1" =  input$kernelNIG_a1,
                              "a2" =  input$kernelNIG_a2,
                              "b1" =  input$kernelNIG_b1,
                              "b2" =  input$kernelNIG_b2,
                              "b3" =  input$kernelNIG_b3,
                              "theta" =  input$kernelNIG_theta),
           "kernelStudent" = list("a" = input$kernelStudent_a,
                                  "b" = input$kernelStudent_b,
                                  "c1" = input$kernelStudent_c1,
                                  "c2" = input$kernelStudent_c2,
                                  "theta" = input$kernelStudent_theta),
           "kernelFatTail" = list("a" = input$kernelFatTail_a,
                                  "b" =input$kernelFatTail_b),
           "kernelGaussian" = list("a" = input$kernelGaussian_a,
                                   "b" = input$kernelGaussian_b),
           "kernelNegExp" = list("a" = input$kernelNegExp_a),
           "kernel2Dt" = list("a" = input$kernel2Dt_a,
                              "b" = input$kernel2Dt_b),
           "kernelInvPowLaw" = list("a" = input$kernelInvPowLaw_a,
                                    "b" = input$kernelInvPowLaw_b),
           "kernelWeibull" = list("a" = input$kernelWeibull,
                                  "b" = input$kernelWeibull),
           "kernelLochSech" = list("a" = input$kernelLochSech_a,
                                   "b" = input$kernelLochSech_b)
           )

    stack_dispersal <- brk_dispersal(landscapeSOURCE_dispersal,
                                     size_raster = input$kernelSizeRaster,
                                     tolerance_square = tolerance_square,
                                     kernel = kernelSWITCH,
                                     kernel.options = kernel.optionsSWITCH,
                                     squared_frame = landscapeSQUAREFRAME_dispersal)
    

    removeModal()

    return(stack_dispersal)

  })
  observe({
    r$stack_dispersal <- stack_dispersal()
  })
  ### SPECIFIC PLOT
  output$plotRunDISPERSAL <- renderPlot({
    input$goRunDispersal
    isolate({
      if(length(stack_dispersal()@layers) >= 9){
        plot(stack_dispersal()[[1:9]])
      } else{
        plot(stack_dispersal()[[1:length(stack_dispersal()@layers)]])
      }
    })
  })
  #
  observe({
    numPlotDISPERSAL <- names(stack_dispersal())
    updateSelectizeInput( session, "numPlotDISPERSAL", choices = numPlotDISPERSAL)
  })
  output$plotDISPERSAL <- renderPlot({
    input$numPlotDISPERSAL
    isolate({
      plot(stack_dispersal()[[input$numPlotDISPERSAL]])
    })
  })
}
    
## To be copied in the UI
# mod_tabItemDispersal_ui("tabItemDispersal_ui_1")
    
## To be copied in the server
# callModule(mod_tabItemDispersal_server, "tabItemDispersal_ui_1")
 



