FROM rocker/tidyverse:3.6.1

RUN apt-get update && apt-get install -y \
    libfftw3-dev \
    libfftw3-bin \
    libgeos-3.5.1 libgeos-c1v5 libgeos-dev \
    libgdal-dev \
    libudunits2-dev
RUN R -e 'install.packages("remotes")'
RUN R -e 'remotes::install_github("r-lib/remotes", ref = "97bbf81")'
RUN R -e 'remotes::install_cran("shiny")'
RUN R -e 'remotes::install_cran("golem")'
RUN R -e 'remotes::install_cran("config")'
RUN R -e 'remotes::install_cran("deldir")'
RUN R -e 'remotes::install_cran("lwgeom")'
RUN R -e 'remotes::install_cran("fasterize")'
RUN R -e 'remotes::install_cran("fftwtools")'
RUN R -e 'remotes::install_cran("mvtnorm")'
RUN R -e 'remotes::install_cran("raster")'
RUN R -e 'remotes::install_cran("RcppArmadillo")'
RUN R -e 'remotes::install_cran("rgeos")'
RUN R -e 'remotes::install_cran("sp")'
RUN R -e 'remotes::install_cran("sf")'
RUN R -e 'remotes::install_cran("leaflet")'
RUN R -e 'remotes::install_cran("htmltools")'
RUN R -e 'remotes::install_cran("shinydashboard")'
RUN R -e 'remotes::install_cran("shinydashboardPlus")'
RUN R -e 'remotes::install_cran("ggplot2")'
RUN R -e 'remotes::install_cran("plotly")'
RUN R -e 'remotes::install_cran("rgdal")'
RUN R -e 'remotes::install_cran("dplyr")'
RUN R -e 'remotes::install_cran("tidyr")'
RUN R -e 'remotes::install_cran("readr")'

# RUN R -e 'remotes::install_cran("briskaR")'
RUN git clone --single-branch --branch develop https://gitlab.paca.inra.fr/biosp/briskaR.git briskaR \
  && cd briskaR \
  && R CMD build --no-build-vignettes --no-manual briskaR \
  && R CMD INSTALL $(ls briskaR_*.tar.gz) \
  && cd .. \
  && rm -r briskaR

COPY briskaRshinyApp_*.tar.gz /app.tar.gz
RUN R -e 'remotes::install_local("/app.tar.gz")'
RUN apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
EXPOSE 3838
CMD  ["R", "-e", "options('shiny.port'=3838,shiny.host='0.0.0.0'); briskaRshinyApp::run_app()"]
