# Building a Prod-Ready, Robust Shiny Application.
# 
# Each step is optional. 
# 

# 2. All along your project

## 2.1 Add modules
## 
# golem::add_module( name = "tabItemStart" ) # Name of the module
# golem::add_module( name = "tabItemLandscape" ) # Name of the module
# golem::add_module( name = "tabItemDispersal" ) # Name of the module
# golem::add_module( name = "tabItemEmission" ) # Name of the module
# golem::add_module( name = "tabItemHost" ) # Name of the module
# golem::add_module( name = "tabItemDevelopment" ) # Name of the module
# golem::add_module( name = "tabItemExposure" ) # Name of the module
# golem::add_module( name = "tabItemDamage" ) # Name of the module
# golem::add_module( name = "tabItemInteractiveMap" ) # Name of the module
# golem::add_module( name = "tabItemDataExplorer" ) # Name of the module
# golem::add_module( name = "tabItemReport" ) # Name of the module

## 2.2 Add dependencies
usethis::use_package( "golem" ) # To call each time you need a new package
usethis::use_package( "briskaR" ) # To call each time you need a new package
usethis::use_package( "sf" ) # To call each time you need a new package
usethis::use_package( "leaflet" ) # To call each time you need a new package
usethis::use_package( "shinydashboard" ) # To call each time you need a new package
usethis::use_package("shinydashboardPlus")
usethis::use_package( "ggplot2" ) # To call each time you need a new package
usethis::use_package( "rgdal" ) # To call each time you need a new package
usethis::use_package( "dplyr" ) # To call each time you need a new package
usethis::use_package( "tidyr" ) # To call each time you need a new package
usethis::use_package( "readr" ) # To call each time you need a new package
usethis::use_package( "shinyWidgets" ) # To call each time you need a new package
usethis::use_package( "plotly" ) # To call each time you need a new package
usethis::use_package( "DT" ) # To call each time you need a new package
## 2.3 Add tests

usethis::use_test( "test_landscape" )

## 2.4 Add a browser button

golem::browser_button()

## 2.5 Add external files

golem::add_js_file( "script" )
golem::add_js_handler( "handlers" )
golem::add_css_file( "custom" )

# 3. Documentation

## 3.1 Vignette
usethis::use_vignette("briskaRshinyApp")
devtools::build_vignettes()

## 3.2 Code coverage
## You'll need GitHub there
usethis::use_github()
usethis::use_travis()
usethis::use_appveyor()

# You're now set! 
# go to dev/03_deploy.R
rstudioapi::navigateToFile("dev/03_deploy.R")
