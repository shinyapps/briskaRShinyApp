# Module UI
  
#' @title   mod_tabItemDamage_ui and mod_tabItemDamage_server
#' @description  A shiny Module.
#'
#' @param id shiny id
#' @param input internal
#' @param output internal
#' @param session internal
#'
#' @rdname mod_tabItemDamage
#'
#' @keywords internal
#' @export 
#' @importFrom shiny NS tagList 
mod_tabItemDamage_ui <- function(id){
  ns <- NS(id)
  tabItem(tabName = "damage",
          title = "Adverse effects",
          # App title ----
          briskaRshinyApp::headerNavigationPage("backExposureTab", "switchInteractiveMapTab"),
          fluidRow(
            shinydashboardPlus::gradientBox(
              title = iconed(" - Information", "info"),
              width = 12,
              gradientColor = "purple", 
              boxToolSize = "xs", 
              collapsible = TRUE,
              closable = FALSE,
              footer_padding = FALSE,
              h4("What to do here?"),
              tags$ul(
                tags$li("(1) check that all previous elements are well defined: source, host, dispersal, deposition and laying site with emergence date, and exposure."), 
                tags$li("(2) select a damage model: a classical sigmoidal dose-response model with LC50 and slope, or a stochastic death survival model."),
                tags$li("(3) provide parameters for the model you selected,"),
                tags$li("(4) run the damage model by clicking on 'Run'."),
              ),
              h4("Check list of previous items"),
              verbatimTextOutput(ns("CheckDamage")),
              hr(),
              h4("Computing adverse effects"),
              p("Once all elements in Check list is OK, this part allows to compute damage probability.
                There are two options, either a classical dose response model based on LC50 and the Slope,
                or through the GUTS modelling approach were the user has to upload the parameters."),
                tags$video(id = ns("videoDamage"),
                         type = "video/mp4",
                         src = "www/damage_capture2.mp4",
                         controls = "controls",
                         height = "300px")
            )
          ),
          fluidRow( # width of fluidrow is 12
            box(title = iconed("Model", "sliders-h"),
                solidHeader = TRUE,
                width = 4, status = "warning",
                strong("Important: make sure to use unit previously defined for exposure:"),
                verbatimTextOutput(ns("unitPollen")),
                  tabsetPanel(id = ns("tabsetDAMAGEsurv"),
                              tabPanel(title = "Dose Response",
                                       value = "DRsurvDAMAGE",
                                       p("For LC50 (lethal concentration for 50% of the individuals), make sure the unit is the same as the one of exposure.
                                         For the parameters provided by default, exposure is in squarred meter
                                         so we convert LC50 in square meter (451*10000)."),
                                       withMathJax(
                                         helpText('$$\\text{Effect}(x) = \\frac{1}{1 + (x/LC_{50})^{\text{slope}}}$$')
                                       ),
                                       numericInput(inputId = ns("LC50DR"),
                                                    label = "LC50",
                                                    value = 451*10^4),
                                       numericInput(inputId = ns("slopeDR"),
                                                    label = "Slope",
                                                    value = -1.76)
                                       
                              ),
                              tabPanel(title = "TKTD model",
                                       value = "GUTSsurvDAMAGE",
                                       p("For each parameter, you can either upload a single value or upload a vector."),
                                       p("For more information about TKTD model, please see the
                                         Scientific Opinion on the state of the art of Toxicokinetic/Toxicodynamic (TKTD) :",
                                         a(href="", "https://www.efsa.europa.eu/fr/efsajournal/pub/5377")),
                                       tabsetPanel(
                                         id = ns("tabsetDAMAGEsurv_GUTS"),
                                         tabPanel(title = "Stochastic Death",
                                                  value = "GUTSsurvDAMAGE_SD",
                                                  h4(tags$b("Toxicokinetic - TK")),
                                                  withMathJax(
                                                    helpText('$$\\frac{D_{int}}{dt} = k_d (C_{ext} - D_{int})$$')
                                                  ),
                                                  numericInput(inputId = ns("kdSD"),
                                                               label = "TK parameter - kd",
                                                               value = 0.3),
                                                  fileInput(ns('kdSDvector'),
                                                            'Choose CSV file - kd',
                                                            accept=c('text/csv', 'text/comma-separated-values,text/plain')),
                                                  h4(tags$b("Toxicodynamic - TD")),
                                                  withMathJax(
                                                    helpText('$$S(t) = \\exp \\left( - \\int_0^t h_b + k_k \\max( D_{int}(\tau) -z) d \\tau \\right)$$')
                                                  ),
                                                  numericInput(inputId = ns("hbSD"),
                                                               label = "Back ground mortality - hb",
                                                               value = 0),
                                                  fileInput(ns('hbSDvector'),
                                                            'Choose CSV file - hb',
                                                            accept=c('text/csv', 'text/comma-separated-values,text/plain')),
                                                  numericInput(inputId = ns("zSD"),
                                                               label = "Threshold - z",
                                                               value = 200),
                                                  fileInput(ns('zSDvector'),
                                                            'Choose CSV file - z',
                                                            accept=c('text/csv', 'text/comma-separated-values,text/plain')),
                                                  numericInput(inputId = ns("kkSD"),
                                                               label = "Killing rate - kk",
                                                               value = 0.02),
                                                  fileInput(ns('kkSDvector'),
                                                            'Choose CSV file - kk',
                                                            accept=c('text/csv', 'text/comma-separated-values,text/plain'))
                                                  
                                         ),
                                         tabPanel(title = "Individual Tolerance",
                                                  value = "GUTSsurvDAMAGE_IT",
                                                  h4(tags$b("Toxicokinetic - TK")),
                                                  withMathJax(
                                                    helpText('$$\\frac{D_{int}}{dt} = k_d (C_{ext} - D_{int})$$')
                                                  ),
                                                  numericInput(inputId = ns("kdIT"),
                                                               label = "TK parameter - kd",
                                                               value = 0.3),
                                                  # fileInput(ns('kdITvector'),
                                                  #           'Choose CSV file - kd',
                                                  #           accept=c('text/csv', 'text/comma-separated-values,text/plain')),
                                                  h4(tags$b("Toxicodynamic - TD")),
                                                  withMathJax(
                                                    helpText('$$S(t) =  h_b + \\frac{1}{1 + (\\max(D_{int}(\tau))/\\alpha)^{\text{\\beta}}}$$')
                                                  ),
                                                  numericInput(inputId = ns("hbIT"),
                                                               label = "Back ground mortality - hb",
                                                               value = 0),
                                                  # fileInput(ns('hbITvector'),
                                                  #           'Choose CSV file - hb',
                                                  #           accept=c('text/csv', 'text/comma-separated-values,text/plain')),
                                                  numericInput(inputId = ns("alphaIT"),
                                                               label = "Threshold - alpha",
                                                               value = 200),
                                                  # fileInput(ns('alphaITvector'),
                                                  #           'Choose CSV file - beta',
                                                  #           accept=c('text/csv', 'text/comma-separated-values,text/plain')),
                                                  numericInput(inputId = ns("betaIT"),
                                                               label = "Killing rate - kk",
                                                               value = 0.02)#•,
                                                  # fileInput(ns('betaITvector'),
                                                  #           'Choose CSV file - kk',
                                                  #           accept=c('text/csv', 'text/comma-separated-values,text/plain'))
                                         )
                                       )
                              ),
                              selected = "DRsurvDAMAGE"
                  ),
                  hr(),
                  actionButton(ns("goRunDAMAGE"),
                               "Run",
                               icon = icon("rocket"),
                               style="color: #fff; background-color: #dd4b39; border-color: #dd4b39")
            ),
            box(title = iconed("Adverse effects", "cogs"),
                solidHeader = TRUE,
                width = 8, status = "success",
                p("The graphic on the left is the histogram showing the probability distribution of the adverse effect
                 (default example is survival probability) along the period cover by source exposure."),
                p("Adverse effect unit is the mortality probability of larvae as provided by the histogram.
                   So here, damage is the daily probability of survival at each laying site."),
                column(width = 6,
                       plotOutput(ns("profileDAMAGE")),
                       p("You can download the data just generated through the CSV and the KML files."),
                       downloadButton(ns("download_DAMAGE"),
                                      "Download Adverse effects CSV",
                                      style="color: #fff; background-color: #33595f; border-color: #052327"),
                       downloadButton(ns("download_DAMAGEkml"),
                                      "Download Adverse effects KML",
                                      style="color: #fff; background-color: #33595f; border-color: #052327")
                ),
                column(width = 6,
                       selectizeInput(ns("ctrlDATEMapDAMAGE"),
                                      label = "Choose Column Date",
                                      choices = NULL,
                                      selected = NULL,
                                      options = list(
                                        placeholder = 'Please select column below',
                                        onInitialize = I('function() { this.setValue(""); }')
                                      )
                       ),
                       tags$ul(
                         tags$li(strong("Blue area are habitats of larvae,")),
                         tags$li(strong("Red area are Bt maize,")),
                         tags$li(strong("Black dots are laying sites,")),
                         tags$li(strong("Legend color is adverse effect of laying sites."))
                       ),
                       plotOutput(ns("mapDAMAGE"))
                )
            )
          ),
          briskaRshinyApp::footerPage()
  )
}
    
# Module Server
    
#' @rdname mod_tabItemDamage
#' @export
#' @keywords internal
    
mod_tabItemDamage_server <- function(input, output, session, r){
  ns <- session$ns
  ### CHECKING
  output$CheckDamage <- renderText({
    CheckDamage = list()
    CheckDamage$landscapeSOURCE= ifelse(is.null(r$landscapeSOURCE), "NO Source", "Source OK")
    CheckDamage$landscapeHOST= ifelse(is.null(r$landscapeHOST), "NO Host", "Host OK")
    CheckDamage$stack_dispersal = ifelse(is.null(r$stack_dispersal), "NO Dispersal", "Dispersal OK")
    CheckDamage$stack_exposure = ifelse(is.null(r$stack_exposure), "NO Deposition", "Deposition OK")
    CheckDamage$individualSITE = ifelse(is.null(r$individualSITE), "NO Laying site", "laying site OK")
    CheckDamage$exposureINDIVIDUAL = ifelse(is.null(r$exposureINDIVIDUAL), "NO Exposure", "Exposure OK")
    return(HTML(paste0(
      "1. ", CheckDamage$landscapeSOURCE, "\n",
      "2. ", CheckDamage$landscapeHOST, "\n",
      "3. ", CheckDamage$stack_dispersal, "\n",
      "4. ", CheckDamage$stack_exposure , "\n",
      "5. ", CheckDamage$individualSITE, "\n",
      "6. ", CheckDamage$exposureINDIVIDUAL))
    )
  })
  ## UNIT
  output$unitPollen <- renderText({ r$unitPollen })
  ### USE EXAMPLE
  observeEvent(input$Xple_DAMAGE,{
    r$individualSITE_dev <- briskaRshinyApp::individualSITE_dev
    r$exposureINDIVIDUAL <-  briskaRshinyApp::exposureINDIVIDUAL
  })
  output$profileEXPOSURE <- renderPlot({
    if(!is.null(r$exposureINDIVIDUAL)){
      DFsumEXPOSURE = r$exposureINDIVIDUAL %>%
        tidyr::unnest(c(Date,EXPOSURE)) %>%
        dplyr::group_by(Date) %>%
        dplyr::summarise(sumEXPOSURE = sum(EXPOSURE))

      ggplot() +
        theme_minimal() +
        labs(x = "Time", y = "Probability density function of Total Exposure") +
        geom_area(data = DFsumEXPOSURE,
                  aes(Date, sumEXPOSURE/sum(sumEXPOSURE)), alpha = 0.5, color = NA, fill = "orange")
    }
  })
  ### INITAL PLOT
  output$profileEXPOSURE <- renderPlot({
    if(!is.null(r$exposureINDIVIDUAL)){
      DFsumEXPOSURE = r$exposureINDIVIDUAL %>%
        tidyr::unnest(c(Date,EXPOSURE)) %>%
        dplyr::group_by(Date) %>%
        dplyr::summarise(sumEXPOSURE = sum(EXPOSURE))
    
      ggplot() +
        theme_minimal() +
        labs(x = "Time", y = "Probability density function of Total Exposure") +
        geom_area(data = DFsumEXPOSURE,
                  aes(Date, sumEXPOSURE/sum(sumEXPOSURE)), alpha = 0.5, color = NA, fill = "orange")
    }
  })
  
  
  ### COMPUTE
  damageINDIVIDUAL <- eventReactive(input$goRunDAMAGE, {
    
    showModal(modalDialog(title = "Run in progress...",
                          "It can take a long time",
                          easyClose = TRUE,
                          footer="blablabla"))
   

    if(input$tabsetDAMAGEsurv == "DRsurvDAMAGE"){
      
      damageLethal = function(x,LC50, slope){
        return(1/(1+(x/LC50)^slope))
      }
      damageINDIVIDUAL <- r$exposureINDIVIDUAL %>% 
        dplyr::mutate(DAMAGE = lapply(EXPOSURE, function(expos){damageLethal(expos, input$LC50DR, input$slopeDR)}))

    }
    if(input$tabsetDAMAGEsurv == "GUTSsurvDAMAGE"){
      
      
      if(input$tabsetDAMAGEsurv_GUTS == "GUTSsurvDAMAGE_SD"){
        model_typeSelect = "SD"
        parameterSelect = list(kk = input$kkSD,
                               kd = input$kdSD,
                               z = input$zSD,
                               hb = input$hbSD)
      }
      if(input$tabsetDAMAGEsurv_GUTS == "GUTSsurvDAMAGE_IT"){
        model_typeSelect = "IT"
        parameterSelect = list(alpha = input$alphaIT,
                               kd = input$kdIT,
                               hb = input$hbIT,
                               beta = input$betaSD)
      }
      # COMPUTE DAMAGE
      damageINDIVIDUAL = r$exposureINDIVIDUAL

      TimeLine = sort(unique(tidyr::unnest(r$exposureINDIVIDUAL, c(Date,EXPOSURE))[["Date"]]))
      damageINDIVIDUAL$TIME = lapply(1:nrow(r$exposureINDIVIDUAL), function(i){
        match(r$exposureINDIVIDUAL$Date[[i]], TimeLine)
      })
      
      damageINDIVIDUAL$DAMAGE = lapply(
        1:nrow(damageINDIVIDUAL),
        function(i){
          pSurvODE(damageINDIVIDUAL$EXPOSURE[[i]],
                   damageINDIVIDUAL$TIME[[i]],
                   model_typeSelect,
                   parameterSelect
                   )
        })
      # model
      modelSD <- function(t, State, parms, input)  {
        with(as.list(c(parms, State)), {
          signal = input(t) # exposure
          dD <- kd * (signal - D)     # internal concentration
          dH <- kk * pmax(D - z, 0) + hb # risk function
          res <- c(dD, dH)
          list(res, hb = hb, signal=signal)
        })
      }
      modelIT <- function(t, State, parms, input)  {
        with(as.list(c(parms, State)), {
          signal = input(t) # exposure
          dD <- kd * (signal - D)     # internal concentration
          dH <- kk * pmax(D - z, 0) + hb # risk function
          res <- c(dD, dH)
          list(res, hb = hb, signal=signal)
        })
      }
      # -----------------------------
      #
      # /!\ real time (date) has to be convert in indice time but should start at 0, that is 0:(length-1) !!!
      #
      pSurvODE <- function(exposure,
                           time,
                           model_type = model_typeSelect,
                           parameter = parameterSelect){
        ## external signal with several rectangle impulses
        signal <- data.frame(times = time, import = exposure)
        sigimp <- stats::approxfun(signal$times, signal$import, method = "linear", rule = 2)
        times = time
        ## The parameters
        if(model_type == "SD"){
          modelFUN = modelSD
        }
        if(model_type == "IT"){
          modelFUN = modelIT
        }
        ## Start values for steady state
        xstart <- c(D = 0,  H = 0)
        ## Solve model
        out <- ode(y = xstart,
                   times = times,
                   func = modelFUN,
                   parms = parameter,
                   input = sigimp)
        df = as.data.frame(out)
        if(model_type == "SD"){
          df$pSurv0 = exp(- df$hb *df$time)
          df$pSurv = exp(- out[, grep("H", colnames(out))] )
        }
        if(model_type == "IT"){
          df$pSurv0 = exp(- df$hb *df$time)
          #df$pSurv = exp(- out[, grep("H", colnames(out))] )
        }
        
        return(df)
      }
      
      DFerror = data.frame("time" = NA, "D" = NA, "H"  = NA, 
                           "hb" = NA, "signal"  = NA, "pSurv0"  = NA, "pSurv"  = NA)
      
      
    }
    
    removeModal()
    
    return(damageINDIVIDUAL)
    
  }) 
  observe({
    r$damageINDIVIDUAL <- damageINDIVIDUAL()
  })
  ### Download DAMAGE CSV
  output$download_DAMAGE <- downloadHandler(
    filename = function() {
      paste("download_DAMAGE_", Sys.Date(), ".csv", sep = "")
    },
    content = function(file) {
      dfDAM = damageINDIVIDUAL() %>%
        dplyr::select(Date, DAMAGE) %>%
        tidyr::unnest(c(Date,DAMAGE))
      readr::write_csv(dfDAM, file)
    }
  )
  ### Download DAMAGE KML
  output$download_DAMAGEkml <- downloadHandler(
    filename = function() {
      paste("download_DAMAGE_", Sys.Date(), ".kml", sep = "")
    },
    content = function(file) {
      dfDAMkml = damageINDIVIDUAL() %>%
        dplyr::select(Date, DAMAGE) %>%
        tidyr::unnest(c(Date,DAMAGE))
      sf::st_write(dfDAMkml, file)
    }
  )
  
  ### HISTOGRAMME DAMAGE
  output$profileDAMAGE <- renderPlot({

    if(!is.null(r$damageINDIVIDUAL)){
      DFdamage = r$damageINDIVIDUAL %>%
        tidyr::unnest(c(Date, DAMAGE)) %>%
        dplyr::group_by(Date) %>%
        dplyr::summarise(mean_DAMAGE = mean(DAMAGE, na.rm = TRUE),
                         q025_DAMAGE = quantile(DAMAGE, probs = 0.025, na.rm = TRUE),
                         q975_DAMAGE = quantile(DAMAGE, probs = 0.975, na.rm = TRUE),
                         min_DAMAGE = min(DAMAGE, na.rm = TRUE),
                         max_DAMAGE = max(DAMAGE, na.rm = TRUE))
      minDateDAMAGE = data.frame(Date = do.call("c", damageINDIVIDUAL()[["Date"]]))

      ggplot() +
        theme_minimal() +
        labs(x = "Time", y = "Probability Distribution of Mortality") +
        geom_line(data = DFdamage,
                       aes(x = Date, y = mean_DAMAGE), color = "red") +
        geom_ribbon(data = DFdamage,
                    aes(x = Date, ymin = q025_DAMAGE, ymax = q975_DAMAGE), alpha = 0.5, color = NA, fill = "grey10") +
        geom_ribbon(data = DFdamage,
                    aes(x = Date, ymin = min_DAMAGE, ymax = max_DAMAGE), alpha = 0.5, color = NA, fill = "grey90") +
        # Little hack to remove error message on input$ctrlDATEMapDAMAGE ....
        geom_vline(xintercept = tryCatch(as.Date(input$ctrlDATEMapDAMAGE), error = function(cond){as.Date(min(minDateDAMAGE$Date))}),
                   color = "red", size = 1, alpha = 0.8)

    }
  })
  
  
  ### PLOT MAP DAMAGE
  observe({
    if(!is.null(r$damageINDIVIDUAL)){
      DFdamage <- tidyr::unnest(r$damageINDIVIDUAL, c(Date, DAMAGE))
      ctrlDATEMapDAMAGE <- sort(unique(DFdamage[["Date"]]))
      updateSelectizeInput( session, "ctrlDATEMapDAMAGE", selected = min(ctrlDATEMapDAMAGE), choices = ctrlDATEMapDAMAGE)
    }
  })
  DFdamage <- reactive({
    if(!is.null(r$damageINDIVIDUAL)){
      DFdamage = r$damageINDIVIDUAL %>%
        tidyr::unnest( c(Date, DAMAGE)) %>%
        dplyr::filter(Date == input$ctrlDATEMapDAMAGE)
    }
  })
  output$mapDAMAGE <- renderPlot({
    
    req(input$ctrlDATEMapDAMAGE)
    ggplot() +
      theme_minimal() +
      scale_color_gradient(low = "green", high = "red", name = "Mortality") +
      geom_sf(data = r$landscapeSOURCE, alpha = 0.2, fill = "red", color = "red") +
      geom_sf(data = r$landscapeHOST,  alpha = 0.2, fill = "blue", color = "blue") +
      geom_sf(data = r$individualSITE_dev) + 
      geom_sf(data = DFdamage(),
              aes(color = tryCatch(DAMAGE, error = function(cond){"black"})), size = 4, alpha = 0.5)

  })
}
    
## To be copied in the UI
# mod_tabItemDamage_ui("tabItemDamage_ui_1")
    
## To be copied in the server
# callModule(mod_tabItemDamage_server, "tabItemDamage_ui_1")
 
